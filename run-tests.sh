//自动化测试脚本 
function showPerFuncDetails()
{
    go tool cover -func=cover.out
}

function generateHtmlCoverageReport()
{
    echo "HTML outputed to ./cover.html"
    go tool cover -html=cover.out -o cover.html
    rm -f cover.out
}

function copyMockConfigurations()
{
    # append mock sections, copy configurations into the home directory
    echo "prepare mocking files..."
    
    mkdir -p confTemp
    cp conf/* confTemp
    
    echo -e "\n[MockRoot]" >> conf/conf.ini
    echo "path = ${GOPATH}/src/myproject/" >> conf/conf.ini
    cp -r conf/ ~
}

function resetMockConfigurations()
{
    echo "removing mocking files"
    rm -rf conf/
    mv confTemp conf
}

##################################################################
#			Display usages
##################################################################
if [ "$#" -eq 0 ]; then
    echo -e "\nUsage (Every run will generate a HTML file reporting the total coverages):"
    echo "    Run every tests, show UNIT(per-package) coverages            -----------------  ./run-tests.sh -unit"
    echo "    Run ALL tests, show ALL(cross-package) coverages             -----------------  ./run-tests.sh -all"
    echo "    Run SINGLE test function, show ALL(cross-package) coverages  -----------------  ./run-tests.sh -run=PACKAGE/FUNC"

    echo -e "\n"
    echo "    Start GoConvey http server (http://ThisHostIp:8080)          -----------------  ./run-tests.sh -ui"
    echo "    Compile GoConvey (needed if your GoConvey is got from others)-----------------  ./run-tests.sh -compile-goconvey"
    
    echo -e "\n"
    echo "    To show PER-FUNCTION coverages, APPEND a -details flag       -----------------  ./run-tests.sh -unit/-all/-run=XXX -details"
    echo -e "\n"

    exit 0
fi

##################################################################
#		Non test related commands
##################################################################
if [ "$#" -eq 1 ] && [ "$1" == "-ui" ]; then
    goconvey -host 0.0.0.0&
    exit 0
    
elif [ "$#" -eq 1 ] && [ "$1" == "-compile-goconvey" ]; then
    echo "compiling goconvey..."
    cd "${GOPATH}/src/git.100tal.com/wangxiao_xueyan_golib/goconvey/goconvey"
    go build goconvey.go
    cp ./goconvey "${GOPATH}/bin/"
    exit 0
    
fi


##################################################################
#	      Run single test function and show coverage
##################################################################

copyMockConfigurations

if [ "$#" -ge 1 ] && [[ "$1" == "-run="* ]]; then

    argValue=$(echo $1 | cut -d'=' -f 2)
    packageName=$(echo $argValue | cut -d'/' -f 1)
    funcName=$(echo $argValue | cut -d'/' -f 2)

    go test "./$packageName" -run $funcName -coverpkg myproject/... -coverprofile=cover.out

    if [ "$#" -eq 2 ] && [ "$2" == "-details" ]; then
	showPerFuncDetails
    fi
        
elif [ "$#" -eq 1 ] && [ "$1" == "-all" ]; then
    #################################################################
    #			All package coverages
    #################################################################
    projectName="myproject"

    echo "geting gocovmerge and running tests..."
    go get github.com/wadey/gocovmerge
    
    declare -a packages=("controller", "main", "model", "sdk", "service", "utils")
    for onePackage in "${packages[@]}"
    do
	go test myproject/... -cover -coverpkg "$projectName/$onePackage" -coverprofile="cover-$onePackage.out" &> /dev/null
    done

    gocovmerge cover*.out > cover.out
    rm -f cover-*.out

    #	This options output detail coverage reports by default
    showPerFuncDetails

elif [ "$#" -ge 1 ] && [ "$1" == "-unit" ]; then
    ###################################################################
    #		Per package coverage
    ###################################################################
    
    echo "running tests..."
    go test myproject/... -coverprofile=cover.out -cover

    if [ "$#" -eq 2 ] && [ "$2" == "-details" ]; then
	showPerFuncDetails
    fi
    
fi

generateHtmlCoverageReport
resetMockConfigurations
