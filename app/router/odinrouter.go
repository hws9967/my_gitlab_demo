//+build odin

package router

import (
	"myproject/app/controller/odincontroller"

	"github.com/gin-gonic/gin"
)

func RegisterRouter(router *gin.Engine) {
	//odinDemo
	redis := router.Group("/demo/redis")
	redis.GET("/set", odincontroller.RedisSetDemo)
	redis.GET("/get", odincontroller.RedisGetDemo)

	mysql := router.Group("/demo/mysql")
	mysql.GET("/insert", odincontroller.MysqlInsertDemo)
	mysql.GET("/select", odincontroller.MysqlSelectDemo)
	mysql.GET("/delete", odincontroller.MysqlDeleteDemo)
}
