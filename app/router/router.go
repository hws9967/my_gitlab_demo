//+build build

package router

import (
	"myproject/app/controller/democontroller"

	"myproject/app/controller/toolcontroller"

	"github.com/gin-gonic/gin"
)

func RegisterRouter(router *gin.Engine) {
	entry := router.Group("/demo")
	entry.GET("/test", democontroller.MyprojectDemo)
	entry.GET("/websocket", democontroller.WebSocketDemo)

	/*
		redis := router.Group("/demo/redis")
		redis.GET("/set", odincontroller.RedisSetDemo)
		redis.GET("/get", odincontroller.RedisGetDemo)

		mysql := router.Group("/demo/mysql")
		mysql.GET("/insert", odincontroller.MysqlInsertDemo)
		mysql.GET("/select", odincontroller.MysqlSelectDemo)
		mysql.GET("/delete", odincontroller.MysqlDeleteDemo)
	*/
	//健康检查
	tool := router.Group("/tool")
	tool.GET("/getIp", toolcontroller.GetIP)
	tool.GET("/getPid", toolcontroller.GetPID)
	tool.GET("/healthCheck", toolcontroller.HealthCheck)
}
