package odincontroller

import (
	"myproject/app/service/odinservice"
	"net/http"

	"git.100tal.com/wangxiao_go_lib/xesGoKit/xesgin"
	"github.com/gin-gonic/gin"
)

func RedisSetDemo(ctx *gin.Context) {
	goCtx := xesgin.TransferToContext(ctx)
	ret, err := odinservice.RedisSet(goCtx)
	if err != nil {
		resp := xesgin.Error(err)
		ctx.JSON(http.StatusOK, resp)
	} else {
		resp := xesgin.Success(ret)
		ctx.JSON(http.StatusOK, resp)
	}
}

func RedisGetDemo(ctx *gin.Context) {
	goCtx := xesgin.TransferToContext(ctx)
	ret, err := odinservice.RedisGet(goCtx)
	if err != nil {
		resp := xesgin.Error(err)
		ctx.JSON(http.StatusOK, resp)
	} else {
		resp := xesgin.Success(ret)
		ctx.JSON(http.StatusOK, resp)
	}
}

func MysqlInsertDemo(ctx *gin.Context) {
	goCtx := xesgin.TransferToContext(ctx)
	ret, err := odinservice.MysqlInsert(goCtx)
	if err != nil {
		resp := xesgin.Error(err)
		ctx.JSON(http.StatusOK, resp)
	} else {
		resp := xesgin.Success(ret)
		ctx.JSON(http.StatusOK, resp)
	}
}

func MysqlSelectDemo(ctx *gin.Context) {
	goCtx := xesgin.TransferToContext(ctx)
	ret, err := odinservice.MysqlSelect(goCtx)
	if err != nil {
		resp := xesgin.Error(err)
		ctx.JSON(http.StatusOK, resp)
	} else {
		resp := xesgin.Success(ret)
		ctx.JSON(http.StatusOK, resp)
	}
}

func MysqlDeleteDemo(ctx *gin.Context) {
	goCtx := xesgin.TransferToContext(ctx)
	ret, err := odinservice.MysqlDelete(goCtx)
	if err != nil {
		resp := xesgin.Error(err)
		ctx.JSON(http.StatusOK, resp)
	} else {
		resp := xesgin.Success(ret)
		ctx.JSON(http.StatusOK, resp)
	}
}
