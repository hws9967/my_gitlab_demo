package toolcontroller

import (
	"net/http"
	"os"

	"git.100tal.com/wangxiao_go_lib/xesGoKit/xesgin"
	"git.100tal.com/wangxiao_go_lib/xesTools/addrutil"
	"github.com/gin-gonic/gin"
)

//GetIP 获取Ip
func GetIP(ctx *gin.Context) {
	addr, err := addrutil.Extract("")
	if err != nil {
		resp := xesgin.Error(err)
		ctx.JSON(http.StatusOK, resp)
	} else {
		resp := xesgin.Success(addr)
		ctx.JSON(http.StatusOK, resp)
	}
}

//GetPID 获取进程id
func GetPID(ctx *gin.Context) {
	resp := xesgin.Success(os.Getpid())
	ctx.JSON(http.StatusOK, resp)
}

//HealthCheck 健康检测
func HealthCheck(ctx *gin.Context) {
	resp := xesgin.Success(os.Getpid())
	ctx.JSON(http.StatusOK, resp)
}
