package odinservice

import (
	"context"

	"git.100tal.com/wangxiao_go_lib/microCommon/micro"
	"git.100tal.com/wangxiao_go_lib/microCommon/micro/proto"
)
//这里加上注释
func RedisSet(ctx context.Context) (ret interface{}, err error) {
	client, _ := micro.NewClient()
	req := &proto.RedisSetRequest{}
	resp := &proto.RedisSetResponse{}
	err = client.RedisSet(ctx, req, resp)
	if err != nil {
		return nil, err
	} else {
		return resp, err
	}
}

func RedisGet(ctx context.Context) (ret interface{}, err error) {
	client, _ := micro.NewClient()
	req := &proto.RedisGetRequest{}
	resp := &proto.RedisGetResponse{}
	err = client.RedisGet(ctx, req, resp)
	if err != nil {
		return nil, err
	} else {
		return resp, err
	}
}

func MysqlInsert(ctx context.Context) (ret interface{}, err error) {
	client, _ := micro.NewClient()
	req := &proto.MysqlInsertRequest{}
	resp := &proto.MysqlInsertResponse{}
	err = client.MysqlInsert(ctx, req, resp)
	if err != nil {
		return nil, err
	} else {
		return resp, err
	}
}

func MysqlSelect(ctx context.Context) (ret interface{}, err error) {
	client, _ := micro.NewClient()
	req := &proto.MysqlSelectRequest{}
	resp := &proto.MysqlSelectResponse{}
	err = client.MysqlSelect(ctx, req, resp)
	if err != nil {
		return nil, err
	} else {
		return resp, err
	}
}

func MysqlDelete(ctx context.Context) (ret interface{}, err error) {
	client, _ := micro.NewClient()
	req := &proto.MysqlDeleteRequest{}
	resp := &proto.MysqlDeleteResponse{}
	err = client.MysqlDelete(ctx, req, resp)
	if err != nil {
		return nil, err
	} else {
		return resp, err
	}
}
